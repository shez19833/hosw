# HoSw Tech Test

The aim is to create a command line tool which reads data from a file, performs simple operation on this data and stores or prints a result. Input files could have different format (csv, yml, xml), but they contain the same data. The result could be stored in a plain text file or printed on stdout. Please see the input files to check the data structure.

## Requirements

- Use the files provided.
- Bootstrap a project with Composer.
- Build the tool.
- Add some tests (any one of the following: unit/integration/functional in TDD/BDD style - it's up to you).
- Create a README.md and add very basic documentation (how to run the tool).
- Push your code into your github account or just send it back to me.

## Logic

We should be able to run the tool from a command line and pass an input parameter and optional output parameter.

- Input parameter is a path to a file that should be processed.
- Output parameter is an optional path to the output file.
- If the output parameter is not provided the result should be printed to `stdout`.

The tool should parse the input file and output the result. The result is a simple sum of `value` property for every _active_ entity.

We should be able to run the tool something like:

```
$ php script.php data/file.yml

# outputs 900
```

or

```
$ php start.php data/file.xml results/result.txt

# creates results/result.txt and puts a number 900 as a content
```

## Assumptions

- File extension describes file type (*.csv, *.yml, *.xml).
- User always has to pass 1 or 2 parameters - input and optionally output.
- File is not empty

## Recommendations

- Try to avoid using full stack frameworks (like Symfony or Laravel). Standalone libraries or components are obviously acceptable.
- Try to follow an OOP approach. Don't be afraid of "over-engineering" that tool. This task obviously is simple and could be done in a few lines of code but we're interested in your OOP knowledge.

## Setup
- clone this repository
- make sure you have php on your local machine if not (homestead/laravel valet or just brew install php)
- cd into the directory and run ```composer install```

### Running
- run ```php start.php data/file.xml```  to get the results printed on command prompt
- run ```php start.php data/file.xml data/output/test.txt``` to 'print' it on a file

## Test
- copy phpunit.dist to phpunit.xml and uncomment the lines 15-17. (these lines should be run once as part of CD/CI)
- run ```vendor/bin/phpunit``` making sure you have done the ```composer install```

## Comments

- could have used a package such as: maatwebsite/excel that automatically reads various formats and import them into the database and then query the database
- if the various data files grew big then would use a file handle to read bit of file would use one of the: https://www.sitepoint.com/performant-reading-big-files-php/ suggestions 
    - I could even parse the results into a separate file so i dont have to process same file repeatedly if the script runs again
- Would have probably taken me a bit less time if I had used laravel for its 'glue' (service container, composer phpunit) and its Facades for File/Storage etc helpers
- Can also have specific validation for each scenario (ie file not existing, file extension etc)
- I would add mocks to the CalculateTest (had some trouble with it so left it for now)
