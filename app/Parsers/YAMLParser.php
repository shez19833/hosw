<?php

namespace App\Parsers;

use Symfony\Component\Yaml\Yaml;
use Tightenco\Collect\Support\Collection;

class YAMLParser implements Parser
{
    protected $filepath;

    /**
     * XMLParser constructor.
     * @param string $filepath
     */
    public function __construct(string $filepath)
    {
        $this->filepath = $filepath;
    }

    /**
     * @return Collection
     */
    public function parse() : Collection
    {
        $array = Yaml::parse(file_get_contents($this->filepath))['users'];

        $array = array_map(function ($item) {
            $item['active'] = var_export($item['active'], true);
            return $item;
        }, $array);

        return collect($array);
    }
}
