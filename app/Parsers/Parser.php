<?php

namespace App\Parsers;

use Tightenco\Collect\Support\Collection;

interface Parser
{
    public function parse() : Collection;
}
