<?php

namespace App\Parsers;

use Tightenco\Collect\Support\Collection;

class XMLParser implements Parser
{
    protected $filepath;

    /**
     * XMLParser constructor.
     * @param string $filepath
     */
    public function __construct(string $filepath)
    {
        $this->filepath = $filepath;
    }

    /**
     * @return Collection
     */
    public function parse() : Collection
    {
        $xml = simplexml_load_string(file_get_contents($this->filepath));

        $array = json_decode(json_encode($xml),TRUE);

        return collect($array['user']);
    }
}
