<?php

namespace App\Parsers;

class ParserFactory
{
    /**
     * @param string $filepath
     * @return CSVParser|XMLParser|YAMLParser
     * @throws \Exception
     */
    public static function getParser(string $filepath)
    {
        $extension = pathinfo($filepath, PATHINFO_EXTENSION);

        switch ($extension) {
            case 'csv' : return new CSVParser($filepath);
            case 'xml' : return new XMLParser($filepath);
            case 'yml' : return new YAMLParser($filepath);
            default: throw new \Exception(
                "Cannot parse file with extension: " . $extension . '. Please use csv, xml or yml files');
        }
    }
}
