<?php

namespace App\Parsers;

use Tightenco\Collect\Support\Collection;

class CSVParser implements Parser
{
    protected $filepath;

    /**
     * CSVParser constructor.
     * @param string $filepath
     */
    public function __construct(string $filepath)
    {
        $this->filepath = $filepath;
    }

    /**
     * @return Collection
     */
    public function parse() : Collection
    {
        $csv = array_map('str_getcsv', file($this->filepath));

        array_walk($csv, function(&$a) use ($csv) {
            $a = array_combine($csv[0], $a);
        });

        array_shift($csv); # remove column header

        return collect($csv);
    }
}
