<?php

namespace App\Commands;

use App\Parsers\ParserFactory;
use Exception;

class Calculate
{
    protected $filepath;
    protected $outputPath = null;
    protected $parser;

    /**
     * @param array $arguments
     * @return string
     * @throws Exception
     */
    public function handle(array $arguments) : string
    {
        if (!$this->validArgumentsPassed($arguments)) {
            return "Please make sure you have specified a valid filename, and is csv, xml or yml file";
        }

        $this->setArguments($arguments);

        $this->parser = ParserFactory::getParser($this->filepath);
        $collection = $this->parser->parse($this->filepath);

        $result = $collection
            ->where('active', 'true')
            ->sum('value');

        return $this->outputPath
            ? $this->writeToFile($result)
            : $result;
    }

    /**
     * @param int $result
     * @return string
     */
    protected function writeToFile(int $result) : string
    {
        $directories = dirname($this->outputPath);

        if (!is_dir($directories)) {
            mkdir($directories, 0777, true);
        }

        file_put_contents($this->outputPath, $result);

        // added this to give some feedback
        return "created a file with result: . $result";
    }

    /**
     * @param array $arguments
     * @return bool
     */
    protected function validArgumentsPassed(array $arguments) : bool
    {
        return count($arguments) >= 1
            && count($arguments) <= 2
            && file_exists($arguments[0])
            && in_array(pathinfo($arguments[0], PATHINFO_EXTENSION), ['csv', 'xml', 'yml']);
    }

    /**
     * @param array $arguments
     * @return void
     */
    protected function setArguments(array $arguments) : void
    {
        $this->filepath = $arguments[0];
        if (isset($arguments[1])) {
            $this->outputPath = $arguments[1];
        }
    }
}
