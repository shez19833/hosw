<?php

use App\Parsers\ParserFactory;
use PHPUnit\Framework\TestCase;

class ParserFactoryTest extends TestCase
{
    /**
     * @dataProvider validFileExtensions
     */
    public function test_i_get_a_correct_parser_depending_on_file_extension($file, $class)
    {
        $this->assertEquals($class, ParserFactory::getParser($file));
    }

    /**
     * @dataProvider invalidFileExtensions
     */
    public function test_i_get_an_exception_if_file_extension_is_not_supported($file)
    {
        $this->expectException(Exception::class);

        ParserFactory::getParser($file);
    }

    public function validFileExtensions()
    {
        return [
            ['test.xml', new App\Parsers\XMLParser('test.xml')],
            ['test.csv', new App\Parsers\CSVParser('test.csv')],
            ['test.yml', new App\Parsers\YAMLParser('test.yml')],
        ];
    }

    public function invalidFileExtensions()
    {
        return [
            ['test.xslx'],
            ['test.jpg'],
            ['test.gif'],
            ['test.txt'],
        ];
    }

}
