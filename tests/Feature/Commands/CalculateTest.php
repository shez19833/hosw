<?php

use App\Commands\Calculate;
use App\Parsers\CSVParser;
use App\Parsers\ParserFactory;
use PHPUnit\Framework\TestCase;

class CalculateTest extends TestCase
{
    protected $fileInputPath = 'tests/data/test.csv';
    protected $fileOutputPath = 'tests/data/output/result.txt';

    public function setUp(): void
    {
        parent::setUp();

        $data = <<<EOF
name,active,value
John,true,500
Mark,true,250
Luke,false,200
EOF;

        // doesnt matter about data
        file_put_contents($this->fileInputPath, $data);
    }

    public function test_i_get_sum_of_all_values_where_active_is_true_for_valid_file()
    {
        $command = new Calculate();
        $result = $command->handle([
            $this->fileInputPath
        ]);

        $this->assertEquals(750, $result);
    }

    public function test_a_result_is_written_to_a_file_if_valid_file_given()
    {
        $command = new Calculate();
        $result = $command->handle([
            $this->fileInputPath,
            $this->fileOutputPath,
        ]);

        $this->assertTrue(file_exists($this->fileOutputPath));

        $this->assertEquals(750, file_get_contents($this->fileOutputPath));
    }

    public function test_an_error_is_returned_if_invalid_file_given()
    {
        $command = new Calculate();
        $result = $command->handle([
            'some/file/file.csv'
        ]);
        $expected = 'Please make sure you have specified a valid filename, and is csv, xml or yml file';

        $this->assertEquals($expected, $result);
    }

    public function tearDown(): void
    {
        unlink($this->fileInputPath);
        parent::tearDown();
    }
}
