<?php

use App\Parsers\XMLParser;
use PHPUnit\Framework\TestCase;

class XMLParserTest extends TestCase
{
    protected $filepath = 'tests/data/test.xml';

    public function setUp(): void
    {
        parent::setUp();

        $data = <<<EOF
<users>
    <user>
        <name>John</name>
        <active>true</active>
        <value>500</value>
    </user>
    <user>
        <name>Mark</name>
        <active>true</active>
        <value>250</value>
    </user>
</users>
EOF;
        file_put_contents($this->filepath, $data);
    }

    public function test_i_get_a_collection_after_parsing_an_xml_file()
    {
        $parser = new XMLParser($this->filepath);
        $contents = $parser->parse();
        $expected = collect([
            ['name' => 'John', 'active' => 'true', 'value' => 500],
            ['name' => 'Mark', 'active' => 'true', 'value' => 250],
        ]);

        $this->assertEquals($expected, $contents);
    }

    public function tearDown(): void
    {
        unlink($this->filepath);
        parent::tearDown();
    }
}
