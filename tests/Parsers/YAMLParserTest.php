<?php

use App\Parsers\YAMLParser;
use PHPUnit\Framework\TestCase;

class YAMLParserTest extends TestCase
{
    protected $filepath = 'tests/data/test.yml';

    public function setUp(): void
    {
        parent::setUp();

        $data = <<<EOF
users:
  - name: John
    active: true
    value: 500
  - name: Mark
    active: true
    value: 250
EOF;
        file_put_contents($this->filepath, $data);
    }

    public function test_i_get_a_collection_after_parsing_a_yml_file()
    {
        $parser = new YAMLParser($this->filepath);
        $contents = $parser->parse();
        $expected = collect([
            ['name' => 'John', 'active' => 'true', 'value' => 500],
            ['name' => 'Mark', 'active' => 'true', 'value' => 250],
        ]);

        $this->assertEquals($expected, $contents);
    }

    public function tearDown(): void
    {
        unlink($this->filepath);
        parent::tearDown();
    }
}
