<?php

use App\Parsers\CSVParser;
use PHPUnit\Framework\TestCase;

class CSVParserTest extends TestCase
{
    protected $filepath = 'tests/data/test.csv';

    public function setUp(): void
    {
        parent::setUp();

        $data = <<<EOF
name,active,value
John,true,500
Mark,true,250
EOF;
        file_put_contents($this->filepath, $data);
    }

    public function test_i_get_a_collection_after_parsing_a_csv_file()
    {
        $parser = new CSVParser($this->filepath);
        $contents = $parser->parse();
        $expected = collect([
            ['name' => 'John', 'active' => 'true', 'value' => 500],
            ['name' => 'Mark', 'active' => 'true', 'value' => 250],
        ]);

        $this->assertEquals($expected, $contents);
    }

    public function tearDown(): void
    {
        unlink($this->filepath);
        parent::tearDown();
    }
}
