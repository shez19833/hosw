<?php

use App\Commands\calculate;

require __DIR__ . '/vendor/autoload.php';

// getting rid of first argument which is script name
array_shift($argv);

$command = new Calculate();
echo $command->handle($argv) . "\n";

